"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _controller = _interopRequireDefault(require("@ember/controller"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _controller["default"].extend({
  actions: {
    discardroute: function discardroute() {
      this.transitionToRoute('jobscheduler');
    }
  }
});

exports["default"] = _default;