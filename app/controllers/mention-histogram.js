import Controller from '@ember/controller';
import $ from 'jquery';
import config from '../config/environment';
import {
    bind
} from '@ember/runloop';

export default Controller.extend({
    data: undefined,
    binNo: 20,
    minvalue: 0,
    maxvalue: 999999,
    network: `N/A`,
    init: function () {
        this._super();
        Ember.run.schedule("afterRender", this, function () {
            var self = this
            var url = config.CALL_HOST + 'mention_highchart'
            $.getJSON(url, function (data) {
                self.set('data', data["message"])
                self.datapopulate()
            })
        });
    },

    datapopulate() {
        var that = this
        Highcharts.chart('container', {
            title: {
                text: 'Mention Histogram'
            },
            xAxis: [{
                title: { text: '' },
                alignTicks: false,
                opposite: true
            }, {
                title: { text: 'Edge Weight (No of Mentions)' },
                alignTicks: false

            }],
            yAxis: [{
                title: { text: '' },
                opposite: true
            }, {
                title: { text: 'Frequency' }
            }],
            plotOptions: {
                histogram: {
                    binWidth: 5
                }
            },
            series: [{
                name: 'Mention Histogram',
                type: 'histogram',
                xAxis: 1,
                yAxis: 1,
                baseSeries: 's1',
                zIndex: 10,
                color: '#002b82',
            },
            {
                name: '',
                type: 'scatter',
                data: that.get('data'),
                id: 's1',
                color: '#3e74d1',
                marker: {
                    radius: 0
                }
            }]
        });
    },


    actions: {
        discardroute() {
            this.set('minvalue', 0)
            this.set('maxvalue', 9999)
            this.set('network', 'N/A')
            this.transitionToRoute('homepage')
        },
        filter(maxval, minvalue, network) {
            var url = config.CALL_HOST + 'filter_mention_highchart'
            var that = this
            $.ajax({
                method: "POST",
                url: url,
                data: {
                    maxvalue: maxval,
                    minvalue: minvalue,
                    network: network
                }
            }).then(bind(this, (result) => {
                $('#container').highcharts().destroy();
                that.set('data', result['message'])
                that.datapopulate()
            }))
        }
    }
});
