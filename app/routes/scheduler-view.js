import Route from '@ember/routing/route';
import { hash } from 'rsvp';

export default Route.extend({
    model(params) {
        const store = this.get('store');
        return hash({
            process: store.find('processsheduler', params.sche_id),
            id:params.sche_id,
        })

    },

    setupController(controller, model) {
        controller.set('process', model.process);
        controller.set('schid',model.id);

        this._super(controller, model);
    }
});
