import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | mention-top-n-handlles', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:mention-top-n-handlles');
    assert.ok(route);
  });
});
