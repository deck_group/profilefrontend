import Controller from '@ember/controller';
import $ from 'jquery';
import config from '../config/environment';
import {
    bind
} from '@ember/runloop';

export default Controller.extend({
    data:undefined,
    minvalue: 0,
    maxvalue: 9999,
    network: `N/A`,
    init: function () {
        this._super();
        Ember.run.schedule("afterRender", this, function () {
            var self = this
            var url = config.CALL_HOST + 'mention_network'
            $.getJSON(url, function (data) {
                self.set('data',data["message"])
                self.datapopulate()
            })
        });
    },
    datapopulate() {
        var that=this
        Highcharts.chart('container', {
            chart: {
                type: 'networkgraph',
                height: '80%'
            },
            title: {
                text: ''
            },
          
            plotOptions: {
                networkgraph: {
                    keys: ['from', 'to', 'color'],
                    layoutAlgorithm: {
                        enableSimulation: true,
                        friction: -0.9
                    }
                }
            },
            series: [{
                dataLabels: {
                    enabled: false,
                    linkFormat: ''
                },
                marker: {
                    fillColor: '#002b82',
                    radius: 5,
                },
                data:that.get('data')
            }]
        });
    },
    actions: {
        discardroute() { 
            this.set('minvalue',0)
            this.set('maxvalue',9999)
            this.set('network','N/A')
            this.transitionToRoute('homepage')
        },
        filter(maxval,minvalue,network){
            var url = config.CALL_HOST + 'filter_mention_network'
            var that = this
            $.ajax({
                method: "POST",
                url: url,
                data: {
                    maxvalue: maxval,
                    minvalue: minvalue,
                    network: network
                }
            }).then(bind(this, (result) => {
                $('#container').highcharts().destroy();
                that.set('data', result['message'])
                that.datapopulate()
            }))
        }
    }
});
