import DS from 'ember-data';
const {
    Model,
    attr
} = DS;

export default Model.extend({
    totalcnt: attr('number'),
    mean: attr('number'),
    std: attr('number'),
    min: attr('number'),
    firstquantile: attr('number'),
    secondquantile: attr('number'),
    thirdquantile: attr('number'),
    max: attr('number'),
    networkname: attr('string'),
    networkid: attr('string')
});
