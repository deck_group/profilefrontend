"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _emberData = _interopRequireDefault(require("ember-data"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Model = _emberData["default"].Model,
    attr = _emberData["default"].attr;

var _default = Model.extend({
  jobname: attr('string'),
  process: attr('string'),
  isweekly: attr('string'),
  ismonthly: attr('string'),
  isdaily: attr('string'),
  ishourly: attr('string'),
  month: attr('string'),
  monthday: attr('string'),
  weekday: attr('string'),
  hour: attr('string'),
  minute: attr('string'),
  second: attr('string'),
  status: attr('string'),
  dayofweek: attr('string')
});

exports["default"] = _default;