import DS from 'ember-data';
const {
    Model,
    attr
} = DS;

export default Model.extend({
    networkname: attr('string'),
    networkid: attr('string')
});
