import Controller from '@ember/controller';
import $ from 'jquery';
import config from '../config/environment';
import {
    bind
} from '@ember/runloop';

export default Controller.extend({
    data:undefined,
    minvalue: 10,
    maxvalue: 9999,
    network: `N/A`,
    init: function () {
        this._super();
        Ember.run.schedule("afterRender", this, function () {
            var self = this
            var url = config.CALL_HOST + 'weighted_network'
            $.getJSON(url, function (data) {
                self.set('data', data["message"])
                self.datapopulate()
            })
        });
    },
    datapopulate() {
        var that = this
        Highcharts.addEvent(
            Highcharts.Series,
            'afterSetOptions',
            function (e) {
                var nodes = {};
                e.options.data.forEach(function (link) {
                
                    if (
                        e.options.id === 'lang-tree'
                    ) {
                        nodes[link[1]] = {
                            id: link[1],
                            marker: {
                                radius: link[2]
                            },


                        }
                    }

                });
                e.options.nodes = Object.keys(nodes).map(function (id) {
                    return nodes[id];
                });
            }
        )
        Highcharts.chart('container', {
            chart: {
                type: 'networkgraph',
                height: '80%'
            },
            title: {
                text: ''
            },

            plotOptions: {
                networkgraph: {
                    keys: ['from', 'to', 'weight'],
                    layoutAlgorithm: {
                        enableSimulation: true,
                        friction: -0.9
                    }
                }
            },
            series: [{
                dataLabels: {
                    enabled: true,
                    linkFormat: ''
                },
                marker: {
                    fillColor: '#002b82',
                },
                id: 'lang-tree',
                data: that.get('data')
            }]
        });
    },
    actions: {
        discardroute() { 
            this.set('minvalue',10)
            this.set('maxvalue',9999)
            this.set('network','N/A')
            this.transitionToRoute('homepage')
        },
        filter(maxval,minvalue,network){
            var url = config.CALL_HOST + 'filter_weighted_network'
            var that = this
            $.ajax({
                method: "POST",
                url: url,
                data: {
                    maxvalue: maxval,
                    minvalue: minvalue,
                    network: network
                }
            }).then(bind(this, (result) => {
                $('#container').highcharts().destroy();
                that.set('data', result['message'])
                that.datapopulate()
            }))
        }
    }
});
