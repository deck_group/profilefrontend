import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
    formclass: `form-group`,
    highlightclass: `col-sm-12 no-highlight`,
    rowclass: `row`,
    colclass: `col-sm-3`,
    colclasstwo: `col-sm-6`,
    btnclass: `btn btn-default`,
    btnLabel: `Sign In`,
    message: undefined,
    router: service(),
    loginid:``,
    password:``,
    actions: {
        submit() {
            this.get('router').transitionTo('homepage')
        },

    }
});
