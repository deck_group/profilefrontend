import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Component.extend({
    store: service(),
    router: service(),
    tagName: `div`,
    classNames: [`sc-pCPhY sc-pbMuv bHDFEb`],
    classOne: `sc-pkgFA kIrMbp`,
    classTwo: `sc-ptBBy kpuSv`,
    classThree: `sc-qPzgd TZTEY`,
    classBTN: `sc-fznAgC keRlOj google-material-icons`,
    classFour: `sc-qXUgY gsTPqK`,
    classImage: `sc-plgA-D dNfjzh`,
    classFive: `sc-pRDlx dHYhnw`,
    classSix: `km-list`,
    classSeven: `sc-puCJS iyLynJ`,
    classA: `sc-puCJS iyLynJ`,
    classHome: `sc-pRtAn kSauRZ sc-qWSYE kIwrRP`,
    classSch: `sc-pRtAn kSauRZ sc-qWSYE kIwrRP`,
    classPredRep: `sc-pRtAn kSauRZ sc-qWSYE kIwrRP`,
    classBorrow: `sc-pRtAn kSauRZ sc-qWSYE kIwrRP`,
    classIcon: `rmwc-icon google-material-icons sc-AxheI dsqNyx`,
    sizevalue: `24px`,
    classBarHome: ``,
    classBarSch: ``,
    classBarBorrow: ``,
    classBarPre: ``,
    expandmention: `expand_more`,
    expandmentionshow: "false",
    expandmentionnetworkshow: "false",
    classBarSchCompute: computed('classBarSch', function () {
        return this.get('classBarSch')
    }),
    init() {
        this._super(...arguments);
        if (localStorage.getItem('item') !== undefined) {
            if (localStorage.getItem('item') === 'jobsch') {
                this.set('classHome', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classSch', 'sc-pRtAn hLEAFZ sc-qWSYE kIwrRP')
                this.set('classBorrow', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classPredRep', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classPredBatch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classBarPre', '')
                this.set('classBarHome', '')
                this.set('classBarSch', 'sc-qOxXJ enJAMb')
                this.set('classBarBorrow', '')
                this.set('classBarMatchPre', '')
            }else{
                this.set('classHome', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classSch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classBorrow', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classPredRep', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classPredBatch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classBarPre', '')
                this.set('classBarHome', '')
                this.set('classBarSch', '')
                this.set('classBarBorrow', '')
                this.set('classBarMatchPre', '') 
            }
        }
    },
    actions: {
        home() {
            this.get('router').transitionTo('homepage')
        },

        mentiongraph(expand) {
            if (expand === "expand_more") {
                this.set("expandmention", `expand_less`)
                this.set("expandmentionshow", "true")
            }
            else {
                this.set("expandmention", `expand_more`)
                this.set("expandmentionshow", "false")
            }
        },
        dataformat() {
            localStorage.setItem('item', undefined)
            this.get('router').transitionTo('data-format')
        },
        mentionhistogram() {
            localStorage.setItem('item', undefined)
            this.get('router').transitionTo('mention-histogram')
        },
        mentionNetwork() {
            localStorage.setItem('item', undefined)
            this.get('router').transitionTo('mention-network')
        },
        mentionNetworkWeight() {
            localStorage.setItem('item', undefined)
            this.get('router').transitionTo('mention-network-weight')
        },
        mentiontopn() {
            localStorage.setItem('item', undefined)
            this.get('router').transitionTo('mention-top-n-handlles')
        },
        mentiontime() {
            localStorage.setItem('item', undefined)
            this.get('router').transitionTo('mention-network-time')
        },
        schedule() {
            localStorage.setItem('item', undefined)
            localStorage.setItem('item', 'jobsch')
            this.get('router').transitionTo('jobscheduler')
         }

    }
});
