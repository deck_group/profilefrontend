import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('login',{ path: '/' });
  this.route('homepage');
  this.route('mention-histogram');
  this.route('mention-network');
  this.route('mention-network-weight');
  this.route('mention-top-n-handlles',{ path: '/top-n-handlers' });
  this.route('mention-network-time');
  this.route('data-format');
  this.route('jobscheduler');
  this.route('scheduler-view', { path: '/scheduler/:sche_id/view' });
});

export default Router;
