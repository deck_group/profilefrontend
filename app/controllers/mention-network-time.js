import Controller from '@ember/controller';
import $ from 'jquery';
import config from '../config/environment';
import {
    bind
} from '@ember/runloop';

export default Controller.extend({
    name: undefined,
    data: undefined,
    name2: undefined,
    data2: undefined,
    network: `N/A`,
    init: function () {
        this._super();
        Ember.run.schedule("afterRender", this, function () {
            var self = this
            var url = config.CALL_HOST + 'timeline'
            $.getJSON(url, function (data) {
                self.set('name', data["names"])
                self.set('data', data["number"])
                self.datapopulate()
            })
            var url2 = config.CALL_HOST + 'cumtimeline'
            $.getJSON(url2, function (data2) {
                self.set('name2', data2["names"])
                self.set('data2', data2["number"])
                self.datapopulate2()
            })
        });
    },
    datapopulate() {
        var that = this
        Highcharts.chart('container', {
            title: {
                text: ''
            },

            subtitle: {
                text: ''
            },


            xAxis: {
                categories: that.get('name')
            },

            series: [{
                data: that.get('data'),
                color: '#002b82'
            }]
        });
    },
    datapopulate2() {
        var that = this
        Highcharts.chart('container2', {
            chart: {
                type: 'area'
            },
            title: {
                text: 'Cumilitive'
            },
            xAxis: {
                categories: that.get('name2')
            },
            tooltip: {
                pointFormat: ''
            },
            plotOptions: {
                area: {
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                }
            },
            series: [{
                data: this.get('data2'),
                color: '#002b82'
            }]
        });
    },
    actions: {
        discardroute() {

        },
        filter(mindate, maxdate, network) {
            var url = config.CALL_HOST + 'filter_timeline'
            var that = this
            $.ajax({
                method: "POST",
                url: url,
                data: {
                    maxvalue: maxdate,
                    minvalue: mindate,
                    network: network
                }
            }).then(bind(this, (data) => {
                $('#container').highcharts().destroy();
                that.set('name', data["names"])
                that.set('data', data["number"])
                that.datapopulate()
            }))

            var url2 = config.CALL_HOST + 'filter_cumtimeline'

            $.ajax({
                method: "POST",
                url: url2,
                data: {
                    maxvalue: maxdate,
                    minvalue: mindate,
                    network: network
                }
            }).then(bind(this, (data) => {
                $('#container2').highcharts().destroy();
                that.set('name2', data["names"])
                that.set('data2', data["number"])
                that.datapopulate2()
            }))


        }
    },

});
