"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _service = require("@ember/service");

var _object = require("@ember/object");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  store: (0, _service.inject)(),
  router: (0, _service.inject)(),
  tagName: "div",
  classNames: ["sc-pCPhY sc-pbMuv bHDFEb"],
  classOne: "sc-pkgFA kIrMbp",
  classTwo: "sc-ptBBy kpuSv",
  classThree: "sc-qPzgd TZTEY",
  classBTN: "sc-fznAgC keRlOj google-material-icons",
  classFour: "sc-qXUgY gsTPqK",
  classImage: "sc-plgA-D dNfjzh",
  classFive: "sc-pRDlx dHYhnw",
  classSix: "km-list",
  classSeven: "sc-puCJS iyLynJ",
  classA: "sc-puCJS iyLynJ",
  classHome: "sc-pRtAn kSauRZ sc-qWSYE kIwrRP",
  classSch: "sc-pRtAn kSauRZ sc-qWSYE kIwrRP",
  classPredRep: "sc-pRtAn kSauRZ sc-qWSYE kIwrRP",
  classBorrow: "sc-pRtAn kSauRZ sc-qWSYE kIwrRP",
  classIcon: "rmwc-icon google-material-icons sc-AxheI dsqNyx",
  sizevalue: "24px",
  classBarHome: "",
  classBarSch: "",
  classBarBorrow: "",
  classBarPre: "",
  expandmention: "expand_more",
  expandmentionshow: "false",
  expandmentionnetworkshow: "false",
  classBarSchCompute: (0, _object.computed)('classBarSch', function () {
    return this.get('classBarSch');
  }),
  init: function init() {
    this._super.apply(this, arguments);

    if (localStorage.getItem('item') !== undefined) {
      if (localStorage.getItem('item') === 'jobsch') {
        this.set('classHome', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classSch', 'sc-pRtAn hLEAFZ sc-qWSYE kIwrRP');
        this.set('classBorrow', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classPredRep', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classPredBatch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classBarPre', '');
        this.set('classBarHome', '');
        this.set('classBarSch', 'sc-qOxXJ enJAMb');
        this.set('classBarBorrow', '');
        this.set('classBarMatchPre', '');
      } else {
        this.set('classHome', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classSch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classBorrow', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classPredRep', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classPredBatch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classBarPre', '');
        this.set('classBarHome', '');
        this.set('classBarSch', '');
        this.set('classBarBorrow', '');
        this.set('classBarMatchPre', '');
      }
    }
  },
  actions: {
    home: function home() {
      this.get('router').transitionTo('homepage');
    },
    mentiongraph: function mentiongraph(expand) {
      if (expand === "expand_more") {
        this.set("expandmention", "expand_less");
        this.set("expandmentionshow", "true");
      } else {
        this.set("expandmention", "expand_more");
        this.set("expandmentionshow", "false");
      }
    },
    dataformat: function dataformat() {
      localStorage.setItem('item', undefined);
      this.get('router').transitionTo('data-format');
    },
    mentionhistogram: function mentionhistogram() {
      localStorage.setItem('item', undefined);
      this.get('router').transitionTo('mention-histogram');
    },
    mentionNetwork: function mentionNetwork() {
      localStorage.setItem('item', undefined);
      this.get('router').transitionTo('mention-network');
    },
    mentionNetworkWeight: function mentionNetworkWeight() {
      localStorage.setItem('item', undefined);
      this.get('router').transitionTo('mention-network-weight');
    },
    mentiontopn: function mentiontopn() {
      localStorage.setItem('item', undefined);
      this.get('router').transitionTo('mention-top-n-handlles');
    },
    mentiontime: function mentiontime() {
      localStorage.setItem('item', undefined);
      this.get('router').transitionTo('mention-network-time');
    },
    schedule: function schedule() {
      localStorage.setItem('item', undefined);
      localStorage.setItem('item', 'jobsch');
      this.get('router').transitionTo('jobscheduler');
    }
  }
});

exports["default"] = _default;