"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _controller = _interopRequireDefault(require("@ember/controller"));

var _service = require("@ember/service");

var _object = require("@ember/object");

var _environment = _interopRequireDefault(require("../config/environment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _controller["default"].extend({
  routerService: (0, _service.inject)('router'),
  statusDict: undefined,
  actions: {
    createroute: function createroute() {},
    view: function view(id) {// this.set('initProg', true)
      // this.transitionToRoute('scheduler-view',id)
      // // this.get('routerService').urlFor('scheduler-view', id)
    },
    logdetail: function logdetail() {
      this.transitionToRoute('logdetail');
    }
  }
});

exports["default"] = _default;