import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';

export default Route.extend({
    store: service(),

    model() {
        const store = this.get('store');
        return hash({
            data: store.findAll('processsheduler'),
            
        })
    },
    setupController(controller, model) {
        controller.set('processshedulers', model.data);
        this._super(controller, model);
    }

});
