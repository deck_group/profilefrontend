import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import config from '../config/environment';

export default Controller.extend({
    routerService: service('router'),
    statusDict: undefined,
    actions: {
        createroute() {
            
        },
        view(id) {
            // this.set('initProg', true)
            // this.transitionToRoute('scheduler-view',id)
            // // this.get('routerService').urlFor('scheduler-view', id)
        },
        logdetail() {
            this.transitionToRoute('logdetail')
        }
    }
});
