"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  tagName: "div",
  classNames: ["sc-oUqyN hruVos"],
  classOne: "sc-oUqyN hruVos",
  classBtn: "sc-fzpkqZ UveTt",
  classSpan: "sc-fzoxKX sc-fzoKki cyTXUp",
  router: (0, _service.inject)(),
  actions: {
    logout: function logout() {
      localStorage.setItem('item', undefined);
      this.get('router').transitionTo('login');
    }
  }
});

exports["default"] = _default;