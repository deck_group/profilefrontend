import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | mention-histogram', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:mention-histogram');
    assert.ok(route);
  });
});
