import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
    tagName: `div`,
    classNames: [`sc-oUqyN hruVos`],
    classOne: `sc-oUqyN hruVos`,
    classBtn: `sc-fzpkqZ UveTt`,
    classSpan: `sc-fzoxKX sc-fzoKki cyTXUp`,
    router: service(),
    actions: {
        logout() {
            localStorage.setItem('item', undefined)
            this.get('router').transitionTo('login')
         }
    }
});
