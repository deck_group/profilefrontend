"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _route = _interopRequireDefault(require("@ember/routing/route"));

var _rsvp = require("rsvp");

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _route["default"].extend({
  store: (0, _service.inject)(),
  model: function model() {
    var store = this.get('store');
    return (0, _rsvp.hash)({
      data: store.findAll('processsheduler')
    });
  },
  setupController: function setupController(controller, model) {
    controller.set('processshedulers', model.data);

    this._super(controller, model);
  }
});

exports["default"] = _default;