import Component from '@ember/component';

export default Component.extend({
    tagName: `tr`,
    isReadonly: false,
    mode: ``,
    label:``,
    flag:``,
    broadcast:false,
});
