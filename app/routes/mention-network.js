import Route from '@ember/routing/route';
import { hash } from 'rsvp';

export default Route.extend({
    model() {
        const store = this.get('store');
        return hash({
            graphstat: store.findAll('graphstat'),
        })

    },
    setupController(controller, model) {
        controller.set('graphstats', model.graphstat);
        this._super(controller, model);
    },
});
