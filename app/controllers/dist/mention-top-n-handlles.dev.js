"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _controller = _interopRequireDefault(require("@ember/controller"));

var _jquery = _interopRequireDefault(require("jquery"));

var _environment = _interopRequireDefault(require("../config/environment"));

var _runloop = require("@ember/runloop");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _controller["default"].extend({
  name: undefined,
  data: undefined,
  topN: 10,
  network: "N/A",
  init: function init() {
    this._super();

    Ember.run.schedule("afterRender", this, function () {
      var self = this;
      var url = _environment["default"].CALL_HOST + 'top_n_handlers';

      _jquery["default"].getJSON(url, function (data) {
        self.set('name', data["names"]);
        self.set('data', data["number"]);
        self.datapopulate();
      });
    });
  },
  datapopulate: function datapopulate() {
    var that = this;
    Highcharts.chart('container', {
      chart: {
        type: 'bar'
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        categories: that.get('name'),
        title: {
          text: null
        }
      },
      yAxis: {
        min: 0
      },
      tooltip: {
        valueSuffix: ' millions'
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      credits: {
        enabled: false
      },
      series: [{
        data: that.get("data"),
        color: '#002b82'
      }]
    });
  },
  actions: {
    discardroute: function discardroute() {
      this.set('topN', 10);
      this.set('network', 'N/A');
      this.transitionToRoute('homepage');
    },
    filter: function filter(topn, network) {
      var url = _environment["default"].CALL_HOST + 'filter_top_n_handlers';
      var that = this;

      _jquery["default"].ajax({
        method: "POST",
        url: url,
        data: {
          topn: topn,
          network: network
        }
      }).then((0, _runloop.bind)(this, function (result) {
        (0, _jquery["default"])('#container').highcharts().destroy();
        that.set('name', result["names"]);
        that.set('data', result["number"]);
        that.set('topN', topn);
        that.datapopulate();
      }));
    }
  }
});

exports["default"] = _default;