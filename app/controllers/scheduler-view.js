import Controller from '@ember/controller';

export default Controller.extend({
    actions: {
        discardroute() {
            this.transitionToRoute('jobscheduler')
        },
    }
});
