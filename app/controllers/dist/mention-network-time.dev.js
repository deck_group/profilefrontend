"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _controller = _interopRequireDefault(require("@ember/controller"));

var _jquery = _interopRequireDefault(require("jquery"));

var _environment = _interopRequireDefault(require("../config/environment"));

var _runloop = require("@ember/runloop");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _controller["default"].extend({
  name: undefined,
  data: undefined,
  name2: undefined,
  data2: undefined,
  network: "N/A",
  init: function init() {
    this._super();

    Ember.run.schedule("afterRender", this, function () {
      var self = this;
      var url = _environment["default"].CALL_HOST + 'timeline';

      _jquery["default"].getJSON(url, function (data) {
        self.set('name', data["names"]);
        self.set('data', data["number"]);
        self.datapopulate();
      });

      var url2 = _environment["default"].CALL_HOST + 'cumtimeline';

      _jquery["default"].getJSON(url2, function (data2) {
        self.set('name2', data2["names"]);
        self.set('data2', data2["number"]);
        self.datapopulate2();
      });
    });
  },
  datapopulate: function datapopulate() {
    var that = this;
    Highcharts.chart('container', {
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        categories: that.get('name')
      },
      series: [{
        data: that.get('data'),
        color: '#002b82'
      }]
    });
  },
  datapopulate2: function datapopulate2() {
    var that = this;
    Highcharts.chart('container2', {
      chart: {
        type: 'area'
      },
      title: {
        text: 'Cumilitive'
      },
      xAxis: {
        categories: that.get('name2')
      },
      tooltip: {
        pointFormat: ''
      },
      plotOptions: {
        area: {
          marker: {
            enabled: false,
            symbol: 'circle',
            radius: 2,
            states: {
              hover: {
                enabled: true
              }
            }
          }
        }
      },
      series: [{
        data: this.get('data2'),
        color: '#002b82'
      }]
    });
  },
  actions: {
    discardroute: function discardroute() {},
    filter: function filter(mindate, maxdate, network) {
      var url = _environment["default"].CALL_HOST + 'filter_timeline';
      var that = this;

      _jquery["default"].ajax({
        method: "POST",
        url: url,
        data: {
          maxvalue: maxdate,
          minvalue: mindate,
          network: network
        }
      }).then((0, _runloop.bind)(this, function (data) {
        (0, _jquery["default"])('#container').highcharts().destroy();
        that.set('name', data["names"]);
        that.set('data', data["number"]);
        that.datapopulate();
      }));

      var url2 = _environment["default"].CALL_HOST + 'filter_cumtimeline';

      _jquery["default"].ajax({
        method: "POST",
        url: url2,
        data: {
          maxvalue: maxdate,
          minvalue: mindate,
          network: network
        }
      }).then((0, _runloop.bind)(this, function (data) {
        (0, _jquery["default"])('#container2').highcharts().destroy();
        that.set('name2', data["names"]);
        that.set('data2', data["number"]);
        that.datapopulate2();
      }));
    }
  }
});

exports["default"] = _default;