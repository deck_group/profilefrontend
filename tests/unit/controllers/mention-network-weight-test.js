import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Controller | mention-network-weight', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let controller = this.owner.lookup('controller:mention-network-weight');
    assert.ok(controller);
  });
});
