"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _controller = _interopRequireDefault(require("@ember/controller"));

var _jquery = _interopRequireDefault(require("jquery"));

var _environment = _interopRequireDefault(require("../config/environment"));

var _runloop = require("@ember/runloop");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _controller["default"].extend({
  data: undefined,
  binNo: 20,
  minvalue: 0,
  maxvalue: 999999,
  network: "N/A",
  init: function init() {
    this._super();

    Ember.run.schedule("afterRender", this, function () {
      var self = this;
      var url = _environment["default"].CALL_HOST + 'mention_highchart';

      _jquery["default"].getJSON(url, function (data) {
        self.set('data', data["message"]);
        self.datapopulate();
      });
    });
  },
  datapopulate: function datapopulate() {
    var that = this;
    Highcharts.chart('container', {
      title: {
        text: 'Mention Histogram'
      },
      xAxis: [{
        title: {
          text: ''
        },
        alignTicks: false,
        opposite: true
      }, {
        title: {
          text: 'Edge Weight (No of Mentions)'
        },
        alignTicks: false
      }],
      yAxis: [{
        title: {
          text: ''
        },
        opposite: true
      }, {
        title: {
          text: 'Frequency'
        }
      }],
      plotOptions: {
        histogram: {
          binWidth: 5
        }
      },
      series: [{
        name: 'Mention Histogram',
        type: 'histogram',
        xAxis: 1,
        yAxis: 1,
        baseSeries: 's1',
        zIndex: 10,
        color: '#002b82'
      }, {
        name: '',
        type: 'scatter',
        data: that.get('data'),
        id: 's1',
        color: '#3e74d1',
        marker: {
          radius: 0
        }
      }]
    });
  },
  actions: {
    discardroute: function discardroute() {
      this.set('minvalue', 0);
      this.set('maxvalue', 9999);
      this.set('network', 'N/A');
      this.transitionToRoute('homepage');
    },
    filter: function filter(maxval, minvalue, network) {
      var url = _environment["default"].CALL_HOST + 'filter_mention_highchart';
      var that = this;

      _jquery["default"].ajax({
        method: "POST",
        url: url,
        data: {
          maxvalue: maxval,
          minvalue: minvalue,
          network: network
        }
      }).then((0, _runloop.bind)(this, function (result) {
        (0, _jquery["default"])('#container').highcharts().destroy();
        that.set('data', result['message']);
        that.datapopulate();
      }));
    }
  }
});

exports["default"] = _default;