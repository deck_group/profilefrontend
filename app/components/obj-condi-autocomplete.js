import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Component.extend({
    classNames: [`row block-link block-link--bordered`],
    tagName: 'div',
    label: `Object Title`,
    classOne: `kernels-teaser__item`,
    classTwo: `kernels-teaser__item-title`,
    inputClass: `form-control required`,
    classTwo_r: `col-sm-2 gfir`,
    classTwo_b: `col-sm-7`,
    lists: ``,
    keyaccess: ``,
    store: service(),
    data: ``,
    modeldata: undefined,
    bus: service("pubsub"),
    model: computed('modeldata',function () {
        return this.get('modeldata')
    }),
    init() {
        this._super(...arguments)
        this.get('bus').on('conditional', this, this.recieve);
    },
    recieve(data){
        this.set('modeldata',data)
        this.get('model')
    },
    actions:{
        selectedOption(data) {
            console.log(data)
        }

    }

});
