"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _controller = _interopRequireDefault(require("@ember/controller"));

var _jquery = _interopRequireDefault(require("jquery"));

var _environment = _interopRequireDefault(require("../config/environment"));

var _runloop = require("@ember/runloop");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _controller["default"].extend({
  data: undefined,
  minvalue: 10,
  maxvalue: 9999,
  network: "N/A",
  init: function init() {
    this._super();

    Ember.run.schedule("afterRender", this, function () {
      var self = this;
      var url = _environment["default"].CALL_HOST + 'weighted_network';

      _jquery["default"].getJSON(url, function (data) {
        self.set('data', data["message"]);
        self.datapopulate();
      });
    });
  },
  datapopulate: function datapopulate() {
    var that = this;
    Highcharts.addEvent(Highcharts.Series, 'afterSetOptions', function (e) {
      var nodes = {};
      e.options.data.forEach(function (link) {
        if (e.options.id === 'lang-tree') {
          nodes[link[1]] = {
            id: link[1],
            marker: {
              radius: link[2]
            }
          };
        }
      });
      e.options.nodes = Object.keys(nodes).map(function (id) {
        return nodes[id];
      });
    });
    Highcharts.chart('container', {
      chart: {
        type: 'networkgraph',
        height: '80%'
      },
      title: {
        text: ''
      },
      plotOptions: {
        networkgraph: {
          keys: ['from', 'to', 'weight'],
          layoutAlgorithm: {
            enableSimulation: true,
            friction: -0.9
          }
        }
      },
      series: [{
        dataLabels: {
          enabled: true,
          linkFormat: ''
        },
        marker: {
          fillColor: '#002b82'
        },
        id: 'lang-tree',
        data: that.get('data')
      }]
    });
  },
  actions: {
    discardroute: function discardroute() {
      this.set('minvalue', 10);
      this.set('maxvalue', 9999);
      this.set('network', 'N/A');
      this.transitionToRoute('homepage');
    },
    filter: function filter(maxval, minvalue, network) {
      var url = _environment["default"].CALL_HOST + 'filter_weighted_network';
      var that = this;

      _jquery["default"].ajax({
        method: "POST",
        url: url,
        data: {
          maxvalue: maxval,
          minvalue: minvalue,
          network: network
        }
      }).then((0, _runloop.bind)(this, function (result) {
        (0, _jquery["default"])('#container').highcharts().destroy();
        that.set('data', result['message']);
        that.datapopulate();
      }));
    }
  }
});

exports["default"] = _default;