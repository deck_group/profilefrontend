import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Component.extend({
    tagName: `div`,
    classNames: [`col-sm-12`],
    classSelection: `rankselection`,
    classA:`col-sm-2`,
    classB:`col-sm-8`,
    store: service(),
    isReadonly:false,
    keyaccess: `name`,
    model: computed(function () {
        console.log(this.get('store').findAll(this.get('modelname')))
        return this.get('store').findAll(this.get('modelname'))
    }),
    actions: {
        selectedOption(value) {
            this.set('data', value)
        }
    }



});
