"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _router = _interopRequireDefault(require("@ember/routing/router"));

var _environment = _interopRequireDefault(require("./config/environment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Router = _router["default"].extend({
  location: _environment["default"].locationType,
  rootURL: _environment["default"].rootURL
});

Router.map(function () {
  this.route('login', {
    path: '/'
  });
  this.route('homepage');
  this.route('mention-histogram');
  this.route('mention-network');
  this.route('mention-network-weight');
  this.route('mention-top-n-handlles', {
    path: '/top-n-handlers'
  });
  this.route('mention-network-time');
  this.route('data-format');
  this.route('jobscheduler');
  this.route('scheduler-view', {
    path: '/scheduler/:sche_id/view'
  });
});
var _default = Router;
exports["default"] = _default;