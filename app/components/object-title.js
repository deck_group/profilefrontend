import Component from '@ember/component';
export default Component.extend({
    isReadonly: false,
    message: undefined,
    format: `^[a-zA-Z0-9\\s.@-_,']*$`,
    isPresence: true,
    leasdingspace: true,
    tagName: 'div',
    classNames:[`row block-link block-link--bordered`],
    title: undefined,
    placeholder: ``,
    label: `Object Title`,
    classOne:`kernels-teaser__item`,
    classTwo:`kernels-teaser__item-title`,
    inputClass: `form-control required`,
    minlength: 5,
    maxlength: 4096,
    setNameError: false,
    actions: {
        clearMessage() {
            this.set('message', undefined)
        },
        checkTitle() {
    
        }
    }
});
