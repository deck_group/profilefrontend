import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | mention-network', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:mention-network');
    assert.ok(route);
  });
});
