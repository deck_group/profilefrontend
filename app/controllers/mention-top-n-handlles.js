import Controller from '@ember/controller';
import $ from 'jquery';
import config from '../config/environment';
import {
    bind
} from '@ember/runloop';


export default Controller.extend({
    name: undefined,
    data: undefined,
    topN:10,
    network:`N/A`,
    init: function () {
        this._super();
        Ember.run.schedule("afterRender", this, function () {
            var self = this
            var url = config.CALL_HOST + 'top_n_handlers'
            $.getJSON(url, function (data) {
                self.set('name', data["names"])
                self.set('data', data["number"])
                self.datapopulate()
            })
        });
    },
    datapopulate() {
        var that = this
        Highcharts.chart('container', {
            chart: {
                type: 'bar'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: that.get('name'),
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0
            },
            tooltip: {
                valueSuffix: ' millions'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                data: that.get("data"),
                color: '#002b82'
            }]
        });
    },
    actions: {
        discardroute() {
            this.set('topN',10)
            this.set('network','N/A')
            this.transitionToRoute('homepage')
         },
        filter(topn,network){
            var url = config.CALL_HOST + 'filter_top_n_handlers'
            var that = this
            $.ajax({
                method: "POST",
                url: url,
                data: {
                    topn: topn,
                    network: network
                }
            }).then(bind(this, (result) => {
                $('#container').highcharts().destroy();
                that.set('name', result["names"])
                that.set('data', result["number"])
                that.set('topN', topn)
                that.datapopulate()
            }))
        }
    }
});
