import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | data-format', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:data-format');
    assert.ok(route);
  });
});
